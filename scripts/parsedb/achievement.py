import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def handle():
    achievement_list = []

    with open(pwd + database + 'CharacterList.json') as f:
        character_list = json.load(f)

    for character in character_list:
        if character['m_Rare'] != 4 or character['m_CharaID'] % 10 != 0:
            continue
        id = str(character['m_CharaID'])
        achievement_list += [{
            'id': int('1%s3%s%s' % (i, id[0:4], id[5:7])),
            'type': 1,
            'level': i,
            'trigger': character['m_CharaID']
        } for i in range(2)]

        achievement_list += [{
            'id': int('13%s%s%s' % (i, id[0:4], id[5:7])),
            'type': 3,
            'level': i,
            'trigger': character['m_CharaID']
        } for i in (1, 3)]

    with open(pwd + database + 'MasterOrbList.json') as f:
        master_orb_list = json.load(f)

    for orb in master_orb_list:
        if orb['m_TitleType'] == -1:
            continue
        achievement_list += [{
            'id': int('20%s%s0000' % (i, orb['m_TitleType']+10)),
            'type': 2,
            'level': i,
            'trigger': orb['m_TitleType']
        } for i in range(1, 4)]

    with open(pwd + database + 'AchievementList.json', 'w') as f:
        json.dump(achievement_list, f)


if __name__ == '__main__':
    handle()
