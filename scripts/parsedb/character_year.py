import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def handle():
    with open(pwd + database + 'CharacterList.json') as f:
        character_list = json.load(f)

    for i, character in enumerate(character_list):
        if i < 192:
            character['year'] = 2017
        elif i < 465:
            character['year'] = 2018
        elif i < 667:
            character['year'] = 2019
        elif i < 897:
            character['year'] = 2020
        elif i < 1099:
            character['year'] = 2021
        else:
            character['year'] = 2022

    with open(pwd + database + 'CharacterList.json', 'w') as f:
        json.dump(character_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
