import os
import re
import json
import requests
import datetime

pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'

time_pattern = r'(?:(\d+)年)?(\d+)月(\d+)日(\d+)(?::|：)(\d+)'
time_range_pattern = time_pattern + r'(?:~|～)' + time_pattern

year = 2018
last_t = datetime.datetime(2017, 12, 30)
seconds_of_month = 30 * 24 * 60 * 60

# Because parsing always failed
skip_event_types = [1016, 1055, 1060]


def get_year(event_type):
    if event_type <= 1029:
        return 2018
    if event_type <= 1057:
        return 2019

    return year


def handle():
    global last_t, year

    with open(pwd + database + 'Events.json') as f:
        event_list = json.load(f)

    db_event_list = requests.get('https://database.kirafan.cn/database/Events.json').json()
    db_events = {
        event['m_EventType']: event
        for event in db_event_list
    }

    for event in event_list:
        if event['m_EventType'] in db_events:
            db_event = db_events[event['m_EventType']]
            event['parts'] = db_event['parts']
            event['startTimes'] = db_event['startTimes']
            event['endTimes'] = db_event['endTimes']

            # parse success in past
            # and then record event time
            if event['parts']:
                last_t = datetime.datetime.strptime(event['startTimes'][0], "%Y-%m-%dT%H:%M:%S%z")
                year = last_t.year
                continue

        event['parts'] = []
        event['startTimes'] = []
        event['endTimes'] = []

        if not event['m_Url'] or event['m_EventType'] in skip_event_types:
            continue

        html = requests.get(event['m_Url']).text

        for p in re.findall(r'(<h1>(?:(?!<h1>|<h2>).|\r|\n)*|<h2>(?:(?!<h1>|<h2>).|\r|\n)*)', html):
            part = re.match(r'(<h1>(.*)</h1>|<h2>(.*)</h2>)', p).groups()[0]
            part = re.findall(r'(トレード|ミッション|超強敵|極|超高難易度|乱戦)', part)
            if not part:
                continue

            part = part[0]
            p = re.sub(r'(?!\r\n)\s', '', p)
            p = re.sub(r'<.*?>', '', p)
            times = re.findall(time_range_pattern, p)
            if not times:
                continue

            if not times[0][0] and datetime.datetime.timestamp(last_t) > datetime.datetime.timestamp(datetime.datetime(year, int(times[0][1]), int(times[0][2]))) + 2 * seconds_of_month:
                year += 1

            for time in times:
                start_time = '%04d-%02d-%02dT%02d:%02d:00+09:00' % (
                    int(time[0] or get_year(event['m_EventType'])), *map(int, time[1:5]))
                end_time = '%04d-%02d-%02dT%02d:%02d:00+09:00' % (
                    int(time[5] or get_year(event['m_EventType'])), *map(int, time[6:10]))

                event['parts'].append(part)
                event['startTimes'].append(start_time)
                event['endTimes'].append(end_time)

            # record event time
            last_t = datetime.datetime.strptime(event['startTimes'][0], "%Y-%m-%dT%H:%M:%S%z")
            year = last_t.year

    with open(pwd + database + 'Events.json', 'w') as f:
        json.dump(event_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
