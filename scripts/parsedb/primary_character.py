import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def priority(character):
    p = 0

    # rare
    if character['m_CharaID'] // 1000000 == 32:
        p += {
            2: 2,
            3: 1,
            4: 0,
        }[character['m_Rare']]
    else:
        p += {
            2: 2,
            3: 0,
            4: 1,
        }[character['m_Rare']]

    # period limited
    p += 3 if character['isPeriodLimited'] or '【' in character['m_Name'] else 0
    p += character['m_CharaID'] * 1e-8

    return p


def handle():
    with open(pwd + database + 'CharacterList.json') as f:
        character_list = json.load(f)
    with open(pwd + database + 'NamedList.json') as f:
        named_list = json.load(f)

    named_character = {}

    for character in character_list:
        named_type = character['m_NamedType']
        character0 = named_character.get(named_type)

        if character0 is None or priority(character0) > priority(character):
            named_character[named_type] = character

    for named in named_list:
        try:
            named['primaryCharacterID'] = named_character[named['m_NamedType']]['m_CharaID']
        except KeyError:
            named['primaryCharacterID'] = None

    with open(pwd + database + 'NamedList.json', 'w') as f:
        json.dump(named_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
