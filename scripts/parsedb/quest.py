import os
import sys
import json
import traceback


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def handle():
    with open(pwd + database + 'QuestWaveList.json') as f:
        quest_wave_list = json.load(f)
    with open(pwd + database + 'ADVLibraryList.json') as f:
        adv_library_list = json.load(f)
    with open(pwd + database + 'TitleList.json') as f:
        title_list = json.load(f)
    with open(pwd + database + 'QuestEnemyList.json') as f:
        quest_enemy_list = json.load(f)
    with open(pwd + database + 'QuestWaveRandomList.json') as f:
        quest_wave_random_list = json.load(f)
    with open(pwd + database + 'QuestWaveDrops.json') as f:
        quest_wave_drop_list = json.load(f)

    adv_libraries = {
        item['m_LibraryListID']: item
        for item in adv_library_list
    }
    titles = {
        item['m_TitleType']: item
        for item in title_list
    }
    quest_enemies = {
        item['m_ID']: item
        for item in quest_enemy_list
    }
    quest_wave_randoms = {
        item['m_ID']: item
        for item in quest_wave_random_list
    }
    quest_wave_drops = {
        item['m_ID']: item
        for item in quest_wave_drop_list
    }

    quest_libraries = {}
    quests = {}

    for quest_wave in quest_wave_list:

        # parse quest libraries
        if quest_wave['m_ID'] in range(10000000, 20000000):  # main
            quest = quest_wave['m_ID'] // 10

            chapter, chapter_start_id = 0, 0
            for chapter_, chapter_start_id_ in {
                0: 11000000,
                1: 11010000,
                2: 11020000,
                3: 11030000,
                4: 11040000,
                5: 11050000,
                6: 11060000,
                7: 11070000,
                8: 11080000,
                9: 11085000,
                100: 12000000,
                101: 12010000,
                102: 12020000,
                103: 12030000,
                104: 12040000,
                105: 12050000,
                106: 12060000,
                107: 12070000,
                108: 12080000,
                109: 12090000
            }.items():
                if quest_wave['m_ID'] < chapter_start_id_:
                    break
                chapter, chapter_start_id = chapter_, chapter_start_id_
            difficulty = quest_wave['m_ID'] // 10 % 10

            quest_library = chapter + difficulty * 10
            section = (quest_wave['m_ID'] - chapter_start_id) // 100 % 100
            wave = quest_wave['m_ID'] % 10

        elif quest_wave['m_ID'] in range(30000000, 40000000):  # event
            quest = quest_wave['m_ID'] // 10

            quest_library = quest_wave['m_ID'] // 1000 % 10000
            section = quest_wave['m_ID'] // 10 % 100
            wave = quest_wave['m_ID'] % 10

        elif quest_wave['m_ID'] in range(50000000, 60000000):  # writer
            quest = quest_wave['m_ID'] // 10

            quest_library = quest_wave['m_ID'] // 10000 % 100 + 2000
            section = quest_wave['m_ID'] // 100 % 100
            wave = quest_wave['m_ID'] % 10

        elif quest_wave['m_ID'] in range(20000, 30000):  # daily: coin
            quest = quest_wave['m_ID'] // 10

            quest_library = 3400
            section = quest_wave['m_ID'] // 10 % 10
            wave = quest_wave['m_ID'] % 10

        elif quest_wave['m_ID'] in range(2001000, 2002000):  # daily: kyouka
            quest = quest_wave['m_ID'] // 10

            quest_library = 3100 + [2, 3, 5, 4, 1, 0][
                quest_wave['m_ID'] // 100 % 10]
            section = quest_wave['m_ID'] // 10 % 10
            wave = quest_wave['m_ID'] % 10

        elif quest_wave['m_ID'] in range(2011000, 2012000):  # daily: shinka
            quest = quest_wave['m_ID'] // 10

            quest_library = 3200 + [2, 3, 5, 4, 1, 0, 100][
                quest_wave['m_ID'] // 100 % 10]
            section = quest_wave['m_ID'] // 10 % 10
            wave = quest_wave['m_ID'] % 10

        elif quest_wave['m_ID'] in range(2021000, 2022000):  # daily: shuurenjou
            quest = quest_wave['m_ID'] // 10

            quest_library = 3500 + [0, 2, 4, 1, 3][
                quest_wave['m_ID'] // 10 % 10]
            section = quest_wave['m_ID'] // 100 % 10
            wave = quest_wave['m_ID'] % 10

        elif quest_wave['m_ID'] in range(2040000, 2041000) or quest_wave['m_ID'] in range(2049000, 2050000):  # challenge
            quest = quest_wave['m_ID'] // 10

            quest_library = 4000 + quest_wave['m_ID'] // 10 % 1000
            section = 1
            wave = quest_wave['m_ID'] % 10

        elif quest_wave['m_ID'] in range(2041000, 2042000):  # ignore
            continue

        elif quest_wave['m_ID'] in range(400000000, 500000000):  # memorial
            quest = quest_wave['m_ID'] // 10

            character = quest_wave['m_ID'] // 10 % 10000000 * 10
            title = character // 1000000 - 10

            quest_library = 5000 + title
            section = character
            wave = quest_wave['m_ID'] % 10

        elif quest_wave['m_ID'] in range(6000000, 7000000):  # craft
            quest = quest_wave['m_ID'] // 10
            title = quest // 1000 - 610

            quest_library = 6000 + title
            section = quest // 10 % 100
            wave = quest_wave['m_ID'] % 10

        else:
            print(quest_wave['m_ID'], 'is not parsed!')
            continue

        # add quest enemy resources
        quest_enemy_resource_ids = set()
        for enemy_id in quest_wave['m_QuestEnemyIDs']:
            try:
                quest_enemy_resource_ids.add(
                    quest_enemies[enemy_id]['m_ResourceID'])
            except KeyError:
                continue
        for random_id in quest_wave['m_QuestRandomIDs']:
            try:
                quest_wave_random = quest_wave_randoms[random_id]
            except KeyError:
                continue
            for enemy_id in quest_wave_random['m_QuestEnemyIDs']:
                try:
                    quest_enemy_resource_ids.add(
                        quest_enemies[enemy_id]['m_ResourceID'])
                except KeyError:
                    continue

        # add quest drop items
        items = {}
        for drop_id in quest_wave['m_DropIDs']:
            for i in range(100):
                try:
                    drop = quest_wave_drops[drop_id*100+i]
                except KeyError:
                    break

                if drop['m_DropItemID'] not in items:
                    items[drop['m_DropItemID']] = {
                        'num': 0.0,
                        'groups': 0.0,
                    }
                items[drop['m_DropItemID']]['num'] += drop['m_DropItemNum'] * \
                    drop['m_DropProbability'] / 100
                items[drop['m_DropItemID']]['groups'] += 1 * \
                    drop['m_DropProbability'] / 100

        for random_id in quest_wave['m_QuestRandomIDs']:
            try:
                quest_wave_random = quest_wave_randoms[random_id]
            except KeyError:
                continue
            for probability, drop_id in zip(
                    quest_wave_random['m_Prob'],
                    quest_wave_random['m_DropIDs']):
                for i in range(100):
                    try:
                        drop = quest_wave_drops[drop_id*100+i]
                    except KeyError:
                        break

                    if drop['m_DropItemID'] not in items:
                        items[drop['m_DropItemID']] = {
                            'num': 0.0,
                            'groups': 0.0,
                        }
                    items[drop['m_DropItemID']]['num'] += drop['m_DropItemNum'] * \
                        drop['m_DropProbability'] / 100 * probability / 100
                    items[drop['m_DropItemID']]['groups'] += 1 * \
                        drop['m_DropProbability'] / 100 * probability / 100

        if quest_library not in quest_libraries:
            quest_libraries[quest_library] = {}
        if section not in quest_libraries[quest_library]:
            quest_libraries[quest_library][section] = quest

        if quest not in quests:
            quests[quest] = {
                'waveIDs': {},
                'enemyResourceIDs': set(),
                'items': {},
                'questLibraryID': quest_library,
                'section': section,
            }
        quests[quest]['waveIDs'][wave] = quest_wave['m_ID']
        quests[quest]['enemyResourceIDs'] = set.union(
            quests[quest]['enemyResourceIDs'], quest_enemy_resource_ids)
        for item_id, item in items.items():
            if item_id not in quests[quest]['items']:
                quests[quest]['items'][item_id] = {
                    'num': 0.0,
                    'groups': 0.0,
                }
            quests[quest]['items'][item_id]['num'] += item['num']
            quests[quest]['items'][item_id]['groups'] += item['groups']

    quest_library_list = []

    for id, values in sorted(quest_libraries.items()):
        try:
            category = id // 1000
            name = icon = badge = None

            if category == 0:  # main
                difficulty = id // 10 % 10
                # try:
                name = adv_libraries[id - difficulty * 10]['m_ListName']
                badge = ['Normal', 'Hard', 'Star'][difficulty]
                icon = "https://texture-asset.kirafan.cn/advlibraryicon/advlibraryicon_%s.png" % (
                    id - difficulty * 10)
                # except KeyError:
                #     name = ''
                #     badge = ''
                #     icon = ''

            elif category == 1:  # event
                name = adv_libraries[id]['m_ListName']
                icon = "https://texture-asset.kirafan.cn/advlibraryicon/advlibraryicon_%s.png" % (
                    id)

            elif category == 2:  # writer
                name = adv_libraries[id]['m_ListName']
                icon = "https://texture-asset.kirafan.cn/advlibraryicon/advlibraryicon_%s.png" % (
                    id)

            elif category == 3:  # daily
                name = {
                    3100: '強化種収穫場・陽',
                    3101: '強化種収穫場・月',
                    3102: '強化種収穫場・炎',
                    3103: '強化種収穫場・水',
                    3104: '強化種収穫場・風',
                    3105: '強化種収穫場・土',
                    3200: '進化珠採掘場・陽',
                    3201: '進化珠採掘場・月',
                    3202: '進化珠採掘場・炎',
                    3203: '進化珠採掘場・水',
                    3204: '進化珠採掘場・風',
                    3205: '進化珠採掘場・土',
                    3300: '進化の認定場',
                    3400: '黄金平原',
                    3500: 'せんしの修練場',
                    3501: 'ナイトの修練場',
                    3502: 'まほうつかいの修練場',
                    3503: 'アルケミストの修練場',
                    3504: 'そうりょの修練場',
                }[id]
                badge = {
                    3100: '日',
                    3101: '月',
                    3102: '火',
                    3103: '水',
                    3104: '木',
                    3105: '土',
                    3200: '日',
                    3201: '月',
                    3202: '火',
                    3203: '水',
                    3204: '木',
                    3205: '土',
                    3300: '常設',
                    3400: '月水金',
                    3500: '月木土',
                    3501: '火土',
                    3502: '水金日',
                    3503: '木日',
                    3504: '火金日',
                }[id]
                icon = "https://texture-asset.kirafan.cn/questgroupicon/questgroupicon_%s.png" % (
                    id % 1000)

            elif category == 4:  # challenge
                # [seg_start, offset, seg_end)
                jump_table = [
                    [4091, 57, 0],  # id: 204033 -> 204091 (2020.11 -> 2020.12)
                    [4900, 800, 0],  # id: 204099 -> 204900 (2021.8 -> 2021.9)
                    [4903, 7, 4911],  # id: 204099 -> 204900 (2021.12(0)-(7) -> 2022.1)
                ]
                month = id % 1000 + 1
                sub_id = None
                for jump_item in jump_table:
                    if id < jump_item[0]:
                        break
                    if jump_item[2] != 0 and id < jump_item[2]:
                        sub_id = id - jump_item[0]
                        month -= sub_id
                        break
                    month -= jump_item[1]

                year, month = month // 12 + 2018, month % 12 + 1
                name = '%s月・強敵チャレンジクエスト' % month
                if sub_id is not None:
                    name = name + "(%s)" % (sub_id + 1)
                badge = '%s.%s' % (year, month)
                icon = "https://texture-asset.kirafan.cn/advlibraryicon/advlibraryicon_%04d%02d00.png" % (
                    year, month)

            elif category == 5:  # memorial
                name = titles[id % 1000]['m_DisplayName']
                icon = "https://texture-asset.kirafan.cn/contenttitlelogo/contentslogo%s.png" % (
                    id % 1000)

            elif category == 6:  # craft
                name = titles[id % 1000]['m_DisplayName']
                icon = "https://texture-asset.kirafan.cn/contenttitlelogo/contentslogo%s.png" % (
                    id % 1000)

            else:
                print('Quest not parsed:', id)

        except:
            print('Quest not parsed:', id)
            traceback.print_exc()

        quest_library_list.append({
            'category': category,
            'id': id,
            'name': name,
            'icon': icon,
            'badge': badge,
            'quests': values,
        })

    quest_list = []

    for quest_id, quest in quests.items():
        wave_ids = sorted(quest['waveIDs'].items())
        enemy_resource_ids = sorted(list(quest['enemyResourceIDs']))
        items = sorted(quest['items'].items())
        quest_list.append({
            'questID': quest_id,
            'waveIDs': [wave for _, wave in wave_ids],
            'enemyResourceIDs': enemy_resource_ids,

            # deprecated
            'items': [{
                'itemID': item_id,
                'itemNum': float('%g' % item['num']),
                'itemGroups': float('%g' % item['groups']),
            } for item_id, item in items],

            'itemIDs': [item_id for item_id, _ in items],
            'itemNums': [float('%g' % item['num']) for _, item in items],
            'itemGroups': [float('%g' % item['groups']) for _, item in items],

            'questLibraryID': quest['questLibraryID'],
            'section': quest['section'],
            'itemIDs': [item_id for item_id, _ in items],
            'itemNums': [float('%g' % item['num']) for _, item in items],
            'itemGroups': [float('%g' % item['groups']) for _, item in items],
        })

    with open(pwd + database + 'QuestLibraryList.json', 'w') as f:
        json.dump(quest_library_list, f)
    with open(pwd + database + 'QuestList.json', 'w') as f:
        json.dump(quest_list, f)


if __name__ == '__main__':
    handle()
