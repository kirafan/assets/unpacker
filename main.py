import os
import re
import sys
import ssl
import time
import zlib
import queue
import random
import urllib
import logging
import argparse
import threading

from unpack import unpack
from download.list import get_asset_bundle
from download.utils import download_file, ab_path_hash

try:
    from decryptor2 import decrypt_to_file
except ImportError:
    sys.path.append('../')
    from decryptor2 import decrypt_to_file


logging.basicConfig(
    level=logging.INFO,
    format='%(levelname)s: %(message)s')

args = argparse.Namespace()

ssl._create_default_https_context = ssl._create_unverified_context

assets = queue.Queue()
errors = queue.Queue()
total = None
threads = []


class Asset:
    def __init__(self, name, hash, path, **kwargs):
        self.name = name
        self.hash = hash
        self.path = path

        self.encrypted = kwargs.get('isEncrypt', False)

        self.muast = '%s/%s.%s' % ('cache', self.name[:-6], self.hash[:8])
        self.url = 'https://asset-krr-prd.star-api.com/%s/Android/%s?t=%s' % (
            args.rvh, urllib.parse.quote(self.path), time.strftime('%Y%m%d%H%M%S'))

    def __str__(self):
        return 'Asset(name=%s, hash=%s)' % (self.name, self.hash)


def do_thread():
    while True:
        if assets.empty():
            return
        asset = assets.get()
        logging.info("%s / %s %s" % (
            total - assets.qsize(), total, asset.name))
        os.makedirs(os.path.dirname(asset.muast), exist_ok=True)
        try:
            data = download_file(asset.url)
            if asset.encrypted:
                decrypt_to_file(data, asset.muast + ".tmp")
            else:
                with open(asset.muast + ".tmp", 'wb') as f:
                    f.write(data)
            unpack(asset.muast + ".tmp", args.type, args.verbose)
            os.rename(asset.muast + ".tmp", asset.muast)
        except Exception as e:
            logging.error(e)
            errors.put((asset, e))


def parse():
    parser = argparse.ArgumentParser(
        usage='python main.py rvh [-cfv] [-t THREADN] path... [--type {png,jpg,icon,json,muast}...]')

    def is_rvh(string):
        try:
            assert len(string) == 32
            int(string, 16)
            return string
        except ValueError:
            raise argparse.ArgumentTypeError(
                'rvh should be a 32-digit hex hash')

    # def is_job(string):
    #     try:
    #         int('0' + string, 16)
    #         return string
    #     except ValueError:
    #         raise argparse.ArgumentTypeError(
    #             'job should be a hex hash')

    parser.add_argument(
        'rvh', type=is_rvh,
        help="resource version hash")
    parser.add_argument(
        'path', nargs='*',
        help="asset paths to download")
    parser.add_argument(
        '-j', dest='job', default='0/1',
        help="pipeline job index")
    parser.add_argument(
        '-f', dest='force', action='store_true', default=False,
        help="force download assets")
    parser.add_argument(
        '--old', dest='old', action='store_true', default=False,
        help="download old assets by specific paths")
    parser.add_argument(
        '-v', dest='verbose', action='store_true', default=False,
        help="print debugging details")
    parser.add_argument(
        '-t', dest='threadn', type=int, default=8,
        help="number of threads")
    parser.add_argument(
        '--type', dest='type', nargs='+', default=['png'], choices=['png', 'jpg', 'icon', 'json', 'muast'],
        help="output texture types")

    return parser.parse_args()


def match_paths(name, paths):
    return list(filter(lambda r: re.match(r, name), paths))


def main():
    global args
    args = parse()
    if args.old:
        for item in args.path:
            asset = Asset(item, '00000000')
            if not args.force and os.path.exists(asset.muast):
                continue
            assets.put(asset)
    else:
        asset_bundle = get_asset_bundle(args.rvh)
        a, b = map(int, args.job.split('/'))

        for item in asset_bundle:
            if not match_paths(item['name'], args.path):
                continue
            asset = Asset(**item)
            if int(asset.path[-32:], 16) % b != a:
                continue
            if not args.force and os.path.exists(asset.muast):
                continue
            assets.put(asset)

    global total
    total = assets.qsize()
    if total == 0:
        logging.info('Everything up-to-date. ')
        return

    for _ in range(args.threadn):
        thread = threading.Thread(target=do_thread)
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

    if errors.empty():
        logging.info('%d errors occurred:' % errors.qsize())
    while not errors.empty():
        asset, e = errors.get()
        logging.info("%s: %s", asset.name, e)


if __name__ == '__main__':
    main()
