import os
import urllib
import urllib.request
import hashlib


def download_file(url, file=None):
    data = urllib.request.urlopen(url).read()
    if file is not None:
        os.makedirs(os.path.abspath(os.path.dirname(file)), exist_ok=True)
        with open(file, 'wb') as f:
            f.write(data)
    return data


hash_append = "u52MNwDsYfszMrPG2DfVPmdM4A7Z79Z5eGdnP2aC2R4Ve7eEGMXgmJ6YfibUt6bUzHKFHHS2bd29M8PnGuPcDbeVmFZ7QZDwtsuuXyfygY2NCKGCQc7bSeCRURdfRV3MVdy7NUsk2zrXRX3pRmeZQ8yCzsP8QCCWHBPjg3DLykAQepxPRQek37pPhuNSAPTm3kjJA9mKbuddcGe5DyzutTu7zuTBrFVd5xp6D5ZNmrVNdGNBRUxzRQD5Bpdz5V7N"


def ab_path_hash(path):
    path = path.replace('\\', '/')
    # Path.GetFileName
    fileName = os.path.basename(path)
    dirName = path.replace(fileName, '')
    result = ''
    if dirName:
        result = result + ab_path_hash_internal(dirName) + '/'
    result = result + ab_path_hash_internal(fileName)
    return result


def ab_path_hash_internal(path):
    path = path + hash_append[0:len(path)]
    return hashlib.md5(path.encode()).hexdigest()
