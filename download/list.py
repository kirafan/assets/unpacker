import os
import time
import json
import unitypack
import json
import zlib
import argparse


try:
    from utils import download_file, ab_path_hash
except ImportError:
    from .utils import download_file, ab_path_hash


def get_asset_bundle(rvh, retry=3, platform='Android'):
    manifest_name = 'manifest.' + rvh[:8]
    vb_name = 'v.b.' + rvh[:8]

    if not os.path.exists(manifest_name):
        url = 'https://asset-krr-prd.star-api.com/%s/%s/%s?t=%s' % (
            rvh, platform, ab_path_hash(platform), time.strftime('%Y%m%d%H%M%S'))

        try:
            download_file(url, manifest_name)  # wget.download(url, out=file)
        except Exception as e:
            print(url)
            if not retry:
                raise e
            return get_asset_bundle(rvh, retry=retry-1)

    if not os.path.exists(vb_name):
        url = 'https://asset-krr-prd.star-api.com/%s/%s/%s?t=%s' % (
            rvh, platform, ab_path_hash('v.b'), time.strftime('%Y%m%d%H%M%S'))

        try:
            download_file(url, vb_name)  # wget.download(url, out=file)
        except Exception as e:
            print(url)
            if not retry:
                raise e
            return get_asset_bundle(rvh, retry=retry-1)

    with open(manifest_name, 'rb') as f:
        asset_bundle = {}
        asset = unitypack.load(f).assets[0]
        asset_bundle_manifest = asset.objects[2].read()
        for item in asset_bundle_manifest['AssetBundleNames']:
            asset_bundle[item[0]] = {'name': item[1]}
        for item in asset_bundle_manifest['AssetBundleInfos']:
            asset_bundle[item[0]]['hash'] = ''.join([
                '%02x' % byte
                for byte in item[1]['AssetBundleHash'].values()
            ])

        asset_bundle = {
            item['name']: item for item in asset_bundle.values()
        }

    with open(vb_name, 'rb') as f:
        vb = json.loads(zlib.decompress(f.read()))

        version_bundle = {
            item['name']: item for item in vb['list']
        }

    for name in asset_bundle:
        path = ab_path_hash(name)
        if path in version_bundle:
            asset_bundle[name]['path'] = path
            asset_bundle[name].update({
                key: value
                for key, value in version_bundle[path].items()
                if key != 'name'
            })

    return list(asset_bundle.values())


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('rvh')
    args = parser.parse_args()

    asset_bundle = get_asset_bundle(args.rvh)
    with open('assetBundle.json', 'w') as f:
        json.dump(asset_bundle, f)
